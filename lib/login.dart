import 'dart:async';
import 'dart:convert';

import 'package:aqueous/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  SharedPreferences sharedPreferences;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final TextEditingController homeserverEditingController =
      TextEditingController();

  final TextEditingController userIDEditingController = TextEditingController();

  final TextEditingController passwordEditingController =
      TextEditingController();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();

    () async {
      sharedPreferences = await SharedPreferences.getInstance();

      setState(() {});
    }();
  }

  Future<void> _login() async {
    if (homeserverEditingController.text.isEmpty ||
        userIDEditingController.text.isEmpty ||
        passwordEditingController.text.isEmpty) {
      final snackBar = SnackBar(content: Text("Field cannot be empty!"));

      _scaffoldKey.currentState.showSnackBar(snackBar);
      return;
    }

    setState(() {
      isLoading = true;
    });

    final url = homeserverEditingController.text + "/_matrix/client/r0/login";

    final resp = await http.post(url,
        body: jsonEncode({
          "type": "m.login.password",
          "user": userIDEditingController.text,
          "password": passwordEditingController.text
        }));

    final jsonResp = jsonDecode(resp.body) as Map<String, dynamic>;

    if (jsonResp.containsKey("error") || jsonResp.containsKey("errcode")) {
      final snackBar = SnackBar(
          content: Text(
              jsonResp["error"] ?? jsonResp["errcode"] ?? "Unknown error"));

      _scaffoldKey.currentState.showSnackBar(snackBar);

      setState(() {
        isLoading = false;
      });
      return;
    }

    final userID = jsonResp["user_id"];
    final accessToken = jsonResp["access_token"];

    if (userID == null || accessToken == null) {
      print("NULL response!");
      print(resp.body);
      return;
    }

    sharedPreferences.setString("homeserver", homeserverEditingController.text);
    sharedPreferences.setString("userID", userID);
    sharedPreferences.setString("accessToken", accessToken);

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => HomePage(title: widget.title)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          children: <Widget>[
            SizedBox(height: 80),
            Column(
              children: <Widget>[
                SvgPicture.asset(
                  "assets/matrix_logo.svg",
                  width: 200,
                ),
                SizedBox(height: 20.0),
                Text("Login to Matrix"),
              ],
            ),
            SizedBox(height: 40),
            TextField(
              controller: homeserverEditingController,
              decoration: InputDecoration(
                labelText: "Server address",
                filled: true,
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: userIDEditingController,
              decoration: InputDecoration(
                labelText: "Username",
                filled: true,
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: passwordEditingController,
              decoration: InputDecoration(
                labelText: "Password",
                filled: true,
              ),
              obscureText: true,
            ),
            ButtonBar(
              children: <Widget>[
                RaisedButton(
                  child: Text(
                    "Login",
                    style: TextStyle(color: isLoading ? null : Colors.white),
                  ),
                  color: isLoading ? null : Theme.of(context).primaryColor,
                  onPressed: isLoading ? null : _login,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
