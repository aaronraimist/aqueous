import 'dart:io';

import 'package:libaqueous/libaqueous.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'dart:convert';

class SqfliteStore implements Store {
  SqfliteStore();

  String _path;

  Database _db;

  Future<void> open() async {
    _path = join(await getDatabasesPath(), "matrix.db");
    _db = await openDatabase(_path, version: 1,
        onCreate: (Database db, int version) async {
      await db
          .execute('CREATE TABLE config (key TEXT PRIMARY KEY, value TEXT)');
      await db.execute('CREATE TABLE roomID (id TEXT PRIMARY KEY)');
      await db.execute(
          'CREATE TABLE roomState (id TEXT PRIMARY KEY, name TEXT, topic TEXT, avatarUrl TEXT, unreadEventsCount INTEGER, notificationCount INTEGER, highlightsCount INTEGER, canonicalAlias TEXT, isDirect INTEGER, directUserID TEXT)');
      await db.execute(
          'CREATE TABLE roomAccountData (id TEXT PRIMARY KEY, tags TEXT)');
      await db.execute(
          'CREATE TABLE roomMembers (id INTEGER PRIMARY KEY AUTOINCREMENT, userID TEXT, roomID TEXT)');
      await db.execute(
          'CREATE TABLE roomHasFullMembers (id TEXT PRIMARY KEY, hasFullMembers INTEGER)');
      await db.execute(
          'CREATE TABLE roomEvent (seq INTEGER, roomID TEXT, eventID TEXT, type TEXT, content TEXT, refreshToken TEXT, originServerTs INTEGER, sender TEXT, stateKey TEXT, unsignedData TEXT, redacts TEXT, mToken TEXT, PRIMARY KEY (seq, roomID, eventID))');
      await db.execute(
          'CREATE TABLE roomEventStartSeq (id TEXT PRIMARY KEY, start INTEGER)');
      await db.execute(
          'CREATE TABLE roomEventEndSeq (id TEXT PRIMARY KEY, end INTEGER)');
      await db
          .execute('CREATE TABLE roomToken (id TEXT PRIMARY KEY, token TEXT)');

      await db.execute('CREATE TABLE userID (id TEXT PRIMARY KEY)');
      await db.execute(
          'CREATE TABLE userState (id TEXT PRIMARY KEY, name TEXT, avatarUrl TEXT)');
    });
  }

  Future<void> close() async {
    await _db?.close();
  }

  Future<void> commit() async {}

  Future<void> clear() async {
    await close();
    await deleteDatabase(_path);
    await open();
  }

  bool get isPermanent => false;

  Future<String> syncToken() async {
    final resp = await _db.query("config",
        columns: ["key", "value"],
        where: "key = ?",
        whereArgs: ["token"],
        limit: 1);
    if (resp.isNotEmpty) return resp.first["value"];
    return null;
  }

  Future<void> setSyncToken(String token) async {
    await _db.insert("config", {"key": "token", "value": token},
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<String>> roomIDs() async {
    return (await _db.query("roomID", columns: ["id"])).map((cols) {
      return cols["id"].toString();
    }).toList();
  }

  Future<bool> containsRoomID(String id) async {
    return (await _db.query("roomID",
                columns: ["id"], where: "id = ?", whereArgs: [id], limit: 1))
            .length >
        0;
  }

  Future<void> addRoomID(String id) async {
    await _db.insert("roomID", {"id": id},
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> addRoomIDBulk(List<String> ids) async {
    final batch = _db.batch();

    for (final id in ids) {
      batch.insert("roomID", {"id": id},
          conflictAlgorithm: ConflictAlgorithm.replace);
    }

    await batch.commit();
  }

  Future<RoomState> roomState(String id) async {
    final state = RoomState()..roomID = id;

    final resp = await _db.query("roomState",
        columns: [
          "id",
          "name",
          "topic",
          "avatarUrl",
          "unreadEventsCount",
          "notificationCount",
          "highlightsCount",
          "canonicalAlias",
          "isDirect",
          "directUserID"
        ],
        where: "id = ?",
        whereArgs: [id],
        limit: 1);

    if (resp.isEmpty) return null;

    final firstResp = resp.first;

    state
      ..name = firstResp["name"]
      ..topic = firstResp["topic"]
      ..avatarUrl = (firstResp["avatarUrl"]?.toString()?.isEmpty ?? true)
          ? null
          : Uri.parse(firstResp["avatarUrl"])
      ..unreadEventsCount = firstResp["unreadEventsCount"]
      ..notificationCount = firstResp["notificationCount"]
      ..highlightsCount = firstResp["highlightsCount"]
      ..canonicalAlias = firstResp["canonicalAlias"]
      ..isDirect = (firstResp["isDirect"] == 1)
      ..directUserID = (firstResp["directUserID"]);

    return state;
  }

  Future<void> storeRoomState(String id, RoomState state) async {
    await _db.insert(
        "roomState",
        {
          "id": id,
          "name": state.name,
          "topic": state.topic,
          "avatarUrl":
              state.avatarUrl == null ? "" : state.avatarUrl.toString(),
          "unreadEventsCount": state.unreadEventsCount,
          "notificationCount": state.notificationCount,
          "highlightsCount": state.highlightsCount,
          "canonicalAlias": state.canonicalAlias,
          "isDirect": state.isDirect,
          "directUserID": state.directUserID,
        },
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> storeRoomStateBulk(Map<String, RoomState> states) async {
    final batch = _db.batch();

    states.forEach((id, state) {
      batch.insert(
          "roomState",
          {
            "id": id,
            "name": state.name,
            "topic": state.topic,
            "avatarUrl":
            state.avatarUrl == null ? "" : state.avatarUrl.toString(),
            "unreadEventsCount": state.unreadEventsCount,
            "notificationCount": state.notificationCount,
            "highlightsCount": state.highlightsCount,
            "canonicalAlias": state.canonicalAlias,
            "isDirect": state.isDirect,
            "directUserID": state.directUserID,
          },
          conflictAlgorithm: ConflictAlgorithm.replace);
    });

    await batch.commit();
  }

  Future<RoomAccountData> roomAccountData(String id) async {
    final accountData = RoomAccountData();

    final resp = await _db.query("roomAccountData",
        where: "id = ?", whereArgs: [id], limit: 1);

    if (resp.isEmpty) return null;

    final firstResp = resp.first;

    accountData..tags = Map<String, double>.from(jsonDecode(firstResp["tags"]));

    return accountData;
  }

  Future<void> storeRoomAccountData(
      String id, RoomAccountData accountData) async {
    await _db.insert(
        "roomAccountData",
        {
          "id": id,
          "tags": jsonEncode(accountData.tags).toString(),
        },
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> storeRoomAccountDataBulk(
      Map<String, RoomAccountData> accountDatas) async {
    final batch = _db.batch();

    accountDatas.forEach((id, accountData) {
      batch.insert(
          "roomAccountData",
          {
            "id": id,
            "tags": jsonEncode(accountData.tags).toString(),
          },
          conflictAlgorithm: ConflictAlgorithm.replace);
    });

    await batch.commit();
  }

  Future<RoomMembers> roomMembers(String id) async {
    final roomMembers = RoomMembers();

    roomMembers.userIDs =
        (await _db.query("roomMembers", where: "roomID = ?", whereArgs: [id]))
            .map((val) {
      return val["userID"].toString();
    }).toList();

    final resp = await _db.query("roomHasFullMembers",
        where: "id = ?", whereArgs: [id], limit: 1);

    if (resp.isEmpty)
      roomMembers.hasFullMembers = false;
    else
      roomMembers.hasFullMembers = (resp.first["hasFullMembers"] == 1);

    return roomMembers;
  }

  Future<void> storeRoomMembers(String id, RoomMembers roomMembers) async {
    if (roomMembers == null || roomMembers.userIDs == null) return;

    final batch = _db.batch();

    batch.delete("roomMembers", where: "roomID = ?", whereArgs: [id]);

    for (final userID in roomMembers.userIDs) {
      batch.insert("roomMembers", {"userID": userID, "roomID": id},
          conflictAlgorithm: ConflictAlgorithm.replace);
    }

    batch.insert("roomHasFullMembers",
        {"id": id, "hasFullMembers": roomMembers.hasFullMembers ? 1 : 0},
        conflictAlgorithm: ConflictAlgorithm.replace);

    await batch.commit();
  }

  Future<void> storeRoomMembersBulk(Map<String, RoomMembers> bulk) async {
    final batch = _db.batch();

    bulk.forEach((id, roomMembers) {
      batch.delete("roomMembers", where: "roomID = ?", whereArgs: [id]);

      for (final userID in roomMembers.userIDs) {
        batch.insert("roomMembers", {"userID": userID, "roomID": id},
            conflictAlgorithm: ConflictAlgorithm.replace);
      }

      batch.insert("roomHasFullMembers",
          {"id": id, "hasFullMembers": roomMembers.hasFullMembers ? 1 : 0},
          conflictAlgorithm: ConflictAlgorithm.replace);
    });

    batch.commit();
  }

  Future<void> storeLiveRoomEvent(Event event) async {
    if (event == null || event.roomID == null || event.eventID == null) {
      print("Event is invalid: " + event.toJson().toString());
      return;
    }

    final endResp = (await _db.query("roomEventEndSeq",
        columns: ["id", "end"],
        where: "id = ?",
        whereArgs: [event.roomID],
        limit: 1));
    var end = endResp.isEmpty ? 0 : endResp.first["end"];

    await _db.insert(
        "roomEvent",
        {
          "seq": ++end,
          "roomID": event.roomID,
          "eventID": event.eventID,
          "type": event.type,
          "content": jsonEncode(event.content),
          "refreshToken": jsonEncode(event.refreshToken),
          "originServerTs": event.originServerTs,
          "sender": event.sender,
          "stateKey": event.stateKey,
          "unsignedData": jsonEncode(event.unsignedData),
          "redacts": event.redacts,
          "mToken": event.mToken,
        },
        conflictAlgorithm: ConflictAlgorithm.replace);

    await _db.insert("roomEventEndSeq", {"id": event.roomID, "end": end},
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> storeRoomEvents(
      String id, TokensChunkEvents tokensChunkEvents, bool isHistory) async {
    var start = 0;
    final startResp = await _db.query("roomEventStartSeq",
        columns: ["id", "start"], where: "id = ?", whereArgs: [id], limit: 1);
    if (startResp.isNotEmpty) {
      start = startResp.first["start"];
    }

    var end = 0;
    final endResp = await _db.query("roomEventEndSeq",
        columns: ["id", "end"], where: "id = ?", whereArgs: [id], limit: 1);
    if (endResp.isNotEmpty) {
      end = endResp.first["end"];
    }

    final batch = _db.batch();

    for (final event in tokensChunkEvents.events) {
      batch.insert("roomEvent", {
        "seq": isHistory ? --start : ++end,
        "roomID": event.roomID,
        "eventID": event.eventID,
        "type": event.type,
        "content": jsonEncode(event.content),
        "refreshToken": jsonEncode(event.refreshToken),
        "originServerTs": event.originServerTs,
        "sender": event.sender,
        "stateKey": event.stateKey,
        "unsignedData": jsonEncode(event.unsignedData),
        "redacts": event.redacts,
        "mToken": event.mToken,
      });
    }

    await batch.commit(noResult: true);

    await _db.insert("roomEventStartSeq", {"id": id, "start": start},
        conflictAlgorithm: ConflictAlgorithm.replace);
    await _db.insert("roomEventEndSeq", {"id": id, "end": end},
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<Event>> getRoomMessages(String id) async {
    return (await _db.rawQuery(
            'SELECT * FROM (SELECT * FROM roomEvent ORDER BY seq DESC LIMIT 20) ORDER BY seq ASC'))
        .map((val) {
      return Event()
        ..type = val["type"]
        ..eventID = val["eventID"]
        ..content = jsonDecode(val["content"])
        ..refreshToken = jsonDecode(val["refreshToken"])
        ..originServerTs = val["originServerTs"]
        ..sender = val["sender"]
        ..stateKey = val["stateKey"]
        ..roomID = id
        ..unsignedData = UnsignedData.fromJson(jsonDecode(val["unsignedData"]))
        ..redacts = val["redacts"]
        ..mToken = val["mToken"];
    });
  }

  Future<void> storeBackToken(String id, String token) async {
    await _db.insert("roomToken", {"id": id, "token": token},
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<TokensChunkEvents> getEarlierMessages(
      String id, String fromToken, int limit) async {
    if (id?.isEmpty ?? true) return null;

    final roomTokenResp = await _db.query("roomToken",
        columns: ["token"], where: "id = ?", whereArgs: [id], limit: 1);
    if (roomTokenResp.isNotEmpty) {
      if (fromToken == roomTokenResp.first["token"]) {
        print("Token reach top");
        return null;
      }
    }

    List<Event> subEventsList = [];

    int subEndSeq = 0;

    if (fromToken?.isEmpty ?? true) {
      final endResp = await _db.query("roomEventEndSeq",
          columns: ["id", "end"], where: "id = ?", whereArgs: [id], limit: 1);
      if (endResp.isNotEmpty) {
        subEndSeq = endResp.first["end"];
        print("Getting latest end seq: " + subEndSeq.toString());
      }
    } else {
      final subEndSeqResp = (await _db.query("roomEvent",
          columns: ["seq"],
          where: "roomID = ? AND mToken = ?",
          whereArgs: [id, fromToken],
          limit: 1,
          orderBy: "seq"));
      if (subEndSeqResp.isNotEmpty) {
        subEndSeq = subEndSeqResp.first["seq"];
      }
      print("Found end seq: " + subEndSeq.toString());
    }

    if (subEndSeq == null) {
      print("Unknown token");
      return null;
    }

    final eventsList = (await _db.query("roomEvent",
            where: "roomID = ? AND seq <= ?",
            whereArgs: [id, subEndSeq],
            orderBy: "seq"))
        .map((val) {
      return Event()
        ..type = val["type"]
        ..eventID = val["eventID"]
        ..content = jsonDecode(val["content"])
        ..refreshToken = jsonDecode(val["refreshToken"])
        ..originServerTs = val["originServerTs"]
        ..sender = val["sender"]
        ..stateKey = val["stateKey"]
        ..roomID = id
        ..unsignedData = UnsignedData.fromJson(jsonDecode(val["unsignedData"]))
        ..redacts = val["redacts"]
        ..mToken = val["mToken"];
    }).toList();

    if (eventsList.length == 0) return null;

    if (eventsList.length < limit) {
      subEventsList = eventsList;
    } else {
      for (final event in eventsList.reversed) {
        subEventsList.add(event);
        if (subEventsList.length >= limit &&
            (event.mToken?.isNotEmpty ?? false)) break;
      }
    }

    if (subEventsList.last.mToken?.isEmpty ?? true) {
      subEventsList.last.mToken =
          (await _db.query("roomToken", where: "id = ?", whereArgs: [id]))
              .first["token"];
    }

    return TokensChunkEvents()
      ..start = subEventsList.first.mToken
      ..end = subEventsList.last.mToken
      ..events = subEventsList;
  }

  Future<void> deleteRoomData(String id) async {
    if (id == null) return;
  }

  Future<void> deleteRoom(String id) async {
    if (id == null) return;

    await deleteRoomData(id);

    await _db.delete("roomID", where: "id = ?", whereArgs: [id]);
  }

  Future<List<String>> userIDs() async {
    return (await _db.query("userID", columns: ["id"])).map((cols) {
      return cols["id"].toString();
    }).toList();
  }

  Future<bool> containsUserID(String id) async {
    return (await _db.query("userID",
                columns: ["id"], where: "id = ?", whereArgs: [id], limit: 1))
            .length >
        0;
  }

  Future<void> addUserID(String id) async {
    await _db.insert("userID", {"id": id},
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> addUserIDBulk(List<String> ids) async {
    final batch = _db.batch();

    for (final id in ids) {
      batch.insert("userID", {"id": id},
          conflictAlgorithm: ConflictAlgorithm.replace);
    }

    await batch.commit();
  }

  Future<UserState> userState(String id) async {
    final state = UserState()..userID = id;

    final resp = await _db.query("userState",
        columns: [
          "id",
          "name",
          "avatarUrl",
        ],
        where: "id = ?",
        whereArgs: [id]);

    if (resp.isEmpty) return null;

    final firstResp = resp.first;

    state
      ..name = firstResp["name"]
      ..avatarUrl = (firstResp["avatarUrl"]?.toString()?.isEmpty ?? true)
          ? null
          : Uri.parse(firstResp["avatarUrl"]);

    return state;
  }

  Future<void> storeUserState(String id, UserState state) async {
    await _db.insert(
        "userState",
        {
          "id": id,
          "name": state.name,
          "avatarUrl":
              state.avatarUrl == null ? "" : state.avatarUrl.toString(),
        },
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> storeUserStateBulk(Map<String, UserState> userStates) async {
    final batch = _db.batch();

    userStates.map((id, state) {
      batch.insert(
          "userState",
          {
            "id": id,
            "name": state.name,
            "avatarUrl":
            state.avatarUrl == null ? "" : state.avatarUrl.toString(),
          },
          conflictAlgorithm: ConflictAlgorithm.replace);
    });

    await batch.commit();
  }
}
