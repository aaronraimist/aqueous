import 'package:aqueous/html_parser.dart';
import 'package:flutter/material.dart';

class Html extends StatelessWidget {
  Html({
    Key key,
    @required this.data,
    this.padding,
    this.backgroundColor,
    this.defaultTextStyle,
    this.onLinkTap,
    this.renderNewlines = false,
    this.linkStyle = const TextStyle(
        decoration: TextDecoration.underline,
        color: Colors.blueAccent,
        decorationColor: Colors.blueAccent),
  }) : super(key: key);

  final String data;
  final EdgeInsetsGeometry padding;
  final Color backgroundColor;
  final TextStyle defaultTextStyle;
  final OnLinkTap onLinkTap;
  final bool renderNewlines;
  final TextStyle linkStyle;

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle.merge(
      style: defaultTextStyle ?? DefaultTextStyle.of(context).style,
      child: HtmlRichTextParser(
        onLinkTap: onLinkTap,
        renderNewlines: renderNewlines,
        html: data,
        linkStyle: linkStyle,
      ),
    );
  }
}
