import 'package:aqueous/html_widget.dart';
import 'package:aqueous/image_provider.dart';
import 'package:flutter/material.dart';
import 'package:libaqueous/libaqueous.dart';

class Bubble extends StatelessWidget {
  Bubble({this.event, this.sender, this.time, this.delivered, this.isMe})
      : assert(sender != null);

  final User sender;
  final Event event;
  final String time;
  final bool delivered, isMe;

  @override
  Widget build(BuildContext context) {
    if (EventType.isStateEvent(event.type)) {
      return _buildState(context);
    }
    if (event.type == EventType.MESSAGE) {
      switch (event.content["msgtype"]) {
        case "m.text":
        case "m.emote":
        case "m.notice":
          return _buildTextMessage(context);
        case "m.image":
          return _buildImageMessage(context);
      }
    }
    return Text(event.content.toString());
  }

  Widget _buildTextMessage(BuildContext context) {
    final icon = delivered ? Icons.done : Icons.done_all;

    if (isMe) {
      return Container(
        padding: EdgeInsets.only(left: 8.0, right: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      blurRadius: .5,
                      spreadRadius: 1.0,
                      color: Colors.black.withOpacity(.12))
                ],
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5.0),
                  bottomLeft: Radius.circular(5.0),
                  bottomRight: Radius.circular(10.0),
                ),
              ),
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 48.0),
                    child: Html(
                      data: _eventToString(event),
                      defaultTextStyle: TextStyle(
                          color: Colors.white,
                          fontStyle: EventType.isStateEvent(event.type)
                              ? FontStyle.italic
                              : FontStyle.normal),
                      linkStyle: TextStyle(
                          color: Colors.white,
                          decoration: TextDecoration.underline),
                      onLinkTap: (_) {},
                    ),
                  ),
                  Positioned(
                    bottom: 0.0,
                    right: 0.0,
                    child: Row(
                      children: <Widget>[
                        Text(time,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 10.0,
                            )),
                        SizedBox(width: 3.0),
                        Icon(
                          icon,
                          size: 12.0,
                          color: Colors.white,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    } else {
      return Container(
        padding: EdgeInsets.only(left: 8.0, right: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                sender?.state?.avatarUrl == null
                    ? CircleAvatar(
                        foregroundColor: Colors.white,
                        backgroundColor: Theme.of(context).primaryColor,
                        child: Text(sender?.state?.name
                                ?.substring(0, 1)
                                ?.toUpperCase() ??
                            ""),
                      )
                    : CircleAvatar(
                        backgroundColor: Colors.transparent,
                        backgroundImage: MatrixImage(
                            sender.client.homeServer, sender.state.avatarUrl,
                            width: 80, height: 80),
                      ),
                Container(
                  width: 8.0,
                ),
                Flexible(
                  child: Container(
                    padding: const EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            blurRadius: .5,
                            spreadRadius: 1.0,
                            color: Colors.black.withOpacity(.12))
                      ],
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(5.0),
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(5.0),
                      ),
                    ),
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 48.0),
                          child: Html(
                            data: _eventToString(event),
                            defaultTextStyle: TextStyle(
                                color: Colors.black,
                                fontStyle: EventType.isStateEvent(event.type)
                                    ? FontStyle.italic
                                    : FontStyle.normal),
                            onLinkTap: (_) {},
                          ),
                        ),
                        Positioned(
                          bottom: 0.0,
                          right: 0.0,
                          child: Row(
                            children: <Widget>[
                              Text(time,
                                  style: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 10.0,
                                  )),
                              SizedBox(width: 3.0),
                              Icon(
                                icon,
                                size: 12.0,
                                color: Colors.black38,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      );
    }
  }

  Widget _buildImageMessage(BuildContext context) {
    return Stack(
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(24.0),
          child: Image(
            image: MatrixImage(
                sender.client.homeServer, Uri.parse(event.content["url"])),
          ),
        ),
        Positioned(
          top: 6.0,
          left: isMe ? null : 6.0,
          right: isMe ? 6.0 : null,
          child: Container(
              padding: const EdgeInsets.all(2.0),
              child: sender?.state?.avatarUrl == null
                  ? CircleAvatar(
                      foregroundColor: Colors.white,
                      backgroundColor: Theme.of(context).primaryColor,
                      child: Text(
                          sender?.state?.name?.substring(0, 1)?.toUpperCase() ??
                              ""),
                    )
                  : CircleAvatar(
                      backgroundColor: Colors.transparent,
                      backgroundImage: MatrixImage(
                          sender.client.homeServer, sender.state.avatarUrl,
                          width: 80, height: 80),
                    ),
              decoration: new BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.6), // border color
                shape: BoxShape.circle,
              )),
        ),
      ],
    );
  }

  Widget _buildState(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        sender?.state?.avatarUrl == null
            ? CircleAvatar(
                radius: 12.0,
                foregroundColor: Colors.white,
                backgroundColor: Theme.of(context).primaryColor,
                child: Text(
                    sender?.state?.name?.substring(0, 1)?.toUpperCase() ?? ""),
              )
            : CircleAvatar(
                radius: 12.0,
                backgroundColor: Colors.transparent,
                backgroundImage: MatrixImage(
                    sender.client.homeServer, sender.state.avatarUrl,
                    width: 80, height: 80),
              ),
        Container(
          width: 4.0,
        ),
        Text(
          _eventToString(event),
          style: TextStyle(color: Theme.of(context).textTheme.caption.color),
        ),
      ],
    );
  }

  String _eventToString(Event event) {
    switch (event.type) {
      case EventType.MESSAGE:
        if (event.content.containsKey("formatted_body")) {
          return "<p>" + event.content["formatted_body"] + "</p>";
        }
        return event.content["body"] ?? "";

      case EventType.STATE_ROOM_MEMBER:
        final stateKey =
            (sender.client.users?.containsKey(event.stateKey) ?? false)
                ? sender.client.users[event.stateKey].state?.name
                : null ?? sender.state?.name ?? "";
        switch (event.content["membership"]) {
          case "join":
            return "$stateKey joined";
          case "leave":
            return "$stateKey left";
        }
        return "$stateKey unknown operation";
      case EventType.REDACTION:
        return "Redaction";
    }
    return "Unknown event";
  }
}
