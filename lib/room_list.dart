import 'package:aqueous/image_provider.dart';
import 'package:flutter/material.dart';
import 'package:libaqueous/libaqueous.dart';

class RoomListView extends StatefulWidget {
  RoomListView({Key key, @required this.client, this.onEnterRoom})
      : super(key: key);

  final Client client;

  final Function(Room) onEnterRoom;

  @override
  State<StatefulWidget> createState() => _RoomListViewState();
}

class _RoomListViewState extends State<RoomListView> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Room>>(
      stream: widget.client?.roomsObservable,
      builder: (BuildContext context, AsyncSnapshot<List<Room>> snapshot) {
        if (snapshot.hasError) return Text('Error: ${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return CircularProgressIndicator();
          case ConnectionState.waiting:
            return CircularProgressIndicator();
          case ConnectionState.active:
            return Expanded(
              child: _buildListViews(context, snapshot.data),
            );
          case ConnectionState.done:
            return Expanded(
              child: _buildListViews(context, snapshot.data),
            );
        }
        return null; // unreachable
      },
    );
  }

  Widget _buildListViews(BuildContext context, List<Room> rooms) {
    final List<Room> favouriteRooms = [];
    int favouriteNotificationCount = 0;
    final List<Room> normalRooms = [];
    int normalNotificationCount = 0;
    final List<Room> directRooms = [];
    int directNotificationCount = 0;
    final List<Room> depriotizedRooms = [];
    int deprioritizedNotificationCount = 0;

    for (final room in rooms) {
      if (room.isFavourite) {
        favouriteRooms.add(room);
        favouriteNotificationCount += room.state.notificationCount ?? 0;
        continue;
      }
      if (room.isLowPriority) {
        depriotizedRooms.add(room);
        deprioritizedNotificationCount += room.state.notificationCount ?? 0;

        continue;
      }
      if (room.state.isDirect) {
        directRooms.add(room);
        directNotificationCount += room.state.notificationCount ?? 0;

        continue;
      }

      normalRooms.add(room);
      normalNotificationCount += room.state.notificationCount ?? 0;
    }

    _sort(favouriteRooms);
    _sort(directRooms);
    _sort(normalRooms);
    _sort(depriotizedRooms);

    return ListView(
      padding: EdgeInsets.all(0.0),
      children: <Widget>[
        ExpansionTile(
          title: Text("Favourites"),
          children: favouriteRooms.map((room) {
            return _buildListTile(context, room);
          }).toList(),
          trailing: favouriteNotificationCount == 0
              ? null
              : _buildNotificationCount(
                  context, favouriteNotificationCount, 24.0),
        ),
        ExpansionTile(
          title: Text("People"),
          children: directRooms.map((room) {
            return _buildListTile(context, room);
          }).toList(),
          trailing: directNotificationCount == 0
              ? null
              : _buildNotificationCount(context, directNotificationCount, 24.0),
        ),
        ExpansionTile(
          title: Text("Rooms"),
          children: normalRooms.map((room) {
            return _buildListTile(context, room);
          }).toList(),
          trailing: normalNotificationCount == 0
              ? null
              : _buildNotificationCount(context, normalNotificationCount, 24.0),
        ),
        ExpansionTile(
          title: Text("Low Priorities"),
          children: depriotizedRooms.map((room) {
            return _buildListTile(context, room);
          }).toList(),
          trailing: deprioritizedNotificationCount == 0
              ? null
              : _buildNotificationCount(
                  context, deprioritizedNotificationCount, 24.0),
        ),
      ],
    );
  }

  Widget _buildListTile(BuildContext context, Room room) {
    return ListTile(
      leading: Stack(
        children: <Widget>[
          room.avatarUrl == null
              ? CircleAvatar(
                  foregroundColor: Colors.white,
                  backgroundColor: Theme.of(context).primaryColor,
                  child: Text(
                      room.displayName?.substring(0, 1)?.toUpperCase() ?? ""),
                )
              : CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: MatrixImage(
                      widget.client.homeServer, room.avatarUrl,
                      width: 80, height: 80),
                ),
          Positioned(
            bottom: 0.0,
            right: 0.0,
            width: 16.0,
            height: 16.0,
            child: (room.state.notificationCount ?? 0) > 0
                ? _buildNotificationCount(
                    context, room.state?.notificationCount ?? 0, 16.0)
                : Container(),
          ),
        ],
      ),
      title: Text(room.displayName ?? "", maxLines: 1, softWrap: false),
      subtitle: Text(room.state.topic ?? "", maxLines: 1, softWrap: false),
      dense: true,
      onTap: () {
        widget.onEnterRoom(room);
        if ((room.timeline?.events?.length ?? 0) < 20) {
          room.getEarlierMessages(20);
        }
        if (!room.members.hasFullMembers) {
          print("Got room without full members.");
          room.fetchMembers();
        }
        Navigator.pop(context);
      },
    );
  }

  Widget _buildNotificationCount(
      BuildContext context, int count, double width) {
    return Container(
      width: width,
      height: width,
      padding: const EdgeInsets.all(2.0),
      child: Container(
        child: Center(
          child: Text(
            count.toString(),
            style: TextStyle(color: Colors.white, fontSize: width / 2),
          ),
        ),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          shape: BoxShape.circle,
        ),
      ),
      decoration: BoxDecoration(
        color: Colors.white, // border color
        shape: BoxShape.circle,
      ),
    );
  }

  void _sort(List<Room> rooms) {
    rooms.sort((r1, r2) {
      if ((r1.state.notificationCount ?? 0) !=
          (r2.state.notificationCount ?? 0)) {
        return -(r1.state.notificationCount ?? 0)
            .compareTo(r2.state.notificationCount ?? 0);
      }
      return r1.displayName.compareTo(r2.displayName);
    });
  }
}
