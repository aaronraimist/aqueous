import 'dart:async';

import 'package:aqueous/bubble.dart';
import 'package:flutter/material.dart';
import 'package:libaqueous/libaqueous.dart';

class ChatPage extends StatefulWidget {
  final Room room;

  ChatPage({Key key, @required this.room}) : super(key: key);

  @override
  State createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  _ChatPageState({Key key});

  final TextEditingController textEditingController = TextEditingController();
  final ScrollController scrollController = ScrollController();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();

    scrollController.addListener(() async {
      if (scrollController.position.pixels >=
          scrollController.position.maxScrollExtent - 100) {
        if (!isLoading) {
          isLoading = true;
          print("Fetching...");
          await widget.room.getEarlierMessages(20);
          isLoading = false;
        }
      }
    });

    widget.room.membersObservable.listen((_) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Scrollbar(
            child: StreamBuilder<RoomTimeline>(
              stream: widget.room.timelineObservable,
              builder:
                  (BuildContext context, AsyncSnapshot<RoomTimeline> snapshot) {
                if (snapshot.hasError) return Text('Error: ${snapshot.error}');
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    return CircularProgressIndicator();
                  case ConnectionState.waiting:
                    return CircularProgressIndicator();
                  case ConnectionState.active:
                    return _buildListView(snapshot.data.events);
                  case ConnectionState.done:
                    return _buildListView(snapshot.data.events);
                }
                return null; // unreachable
              },
            ),
          ),
        ),
        _buildTextInput(),
      ],
    );
  }

  Widget _buildItem(Event event) {
    final dateTime = DateTime.fromMillisecondsSinceEpoch(event.originServerTs);
    final time =
        "${dateTime.hour.toString().padLeft(2, '0')}:${dateTime.minute.toString().padLeft(2, '0')}";

    if (event.sender == widget.room.client.userID) {
      return Container(
        padding: EdgeInsets.only(top: 4.0, bottom: 4.0),
        child: Bubble(
            event: event,
            sender: widget.room.client.users[event.sender] ??
                User(widget.room.client, null),
            isMe: true,
            time: time,
            delivered: true),
      );
    }
    return Container(
      padding: EdgeInsets.only(top: 4.0, bottom: 4.0),
      child: Bubble(
          event: event,
          sender: widget.room.client.users[event.sender] ??
              User(widget.room.client, null),
          isMe: false,
          time: time,
          delivered: true),
    );
  }

  Widget _buildListView(List<Event> events) {
    final revEvents = events.reversed.toList();
    return ListView.builder(
      padding: EdgeInsets.all(8.0),
      reverse: true,
      controller: scrollController,
      itemCount: revEvents.length,
      physics:
      const AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
      itemBuilder: (BuildContext ctx, int index) {
        return _buildItem(revEvents[index]);
      },
    );
  }

  Widget _buildTextInput() {
    return Container(
        margin: EdgeInsets.all(8.0),
        child: Row(
          children: <Widget>[
            Flexible(
              child: TextField(
                controller: textEditingController,
                decoration:
                    InputDecoration.collapsed(hintText: "Send a message..."),
              ),
            ),
            IconButton(
              padding: EdgeInsets.all(0.0),
              icon: Icon(Icons.send),
              color: Theme.of(context).primaryColor,
              onPressed: () async =>
                  await _onSendMessage(textEditingController.text),
            ),
          ],
        ),
        padding: EdgeInsets.only(left: 20.0, right: 4.0, top: 4.0, bottom: 4.0),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                blurRadius: .5,
                spreadRadius: 1.0,
                color: Colors.black.withOpacity(.12))
          ],
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(32)),
        ));
  }

  Future<void> _onSendMessage(String content) async {
    if (content.trim() != '') {
      textEditingController.clear();
    } else {
      return;
    }
    await widget.room.postPlainText(content);
  }
}
