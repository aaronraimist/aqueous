import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView(
        padding: EdgeInsets.symmetric(horizontal: 36.0),
        children: <Widget>[
          SizedBox(height: 80),
          Column(
            children: <Widget>[
              SvgPicture.asset(
                "assets/matrix_logo.svg",
                width: 200,
              ),
              SizedBox(height: 32.0),
              Text(
                "Greetings",
                style: TextStyle(fontSize: 28.0),
              ),
              SizedBox(height: 20.0),
              Text("Welcome to Matrix, a new era of instant messaging."),
              SizedBox(height: 20.0),
              Text("To start chatting, select a room from the side bar."),
            ],
          ),
        ],
      ),
    );
  }
}
